# Space Invanders

O projeto do jogo Space Invaders, feito durante o terceiro período da faculdade, é uma implementação do jogo original, seguindo a risca as mecanicas principais e mais algumas implementações bônus para deixar o jogo mais divertido.

<img src="https://gitlab.com/gabsfanaia/space-invaders/-/raw/main/sprites/MENU.png" alt="Texto alternativo">

## Arquivos

- ship.h e ship.c
- enemy.h e enemy.c
- obstacle.h e obstacle.c
- mothership.h e mothership.c
- shoots_and_collision.h e shoots_and_collision.c
- additional.h e additional.c

## Funcionalidades
   
### Menu
- Existência de um menu principal que contém um tutorial e o start do jogo. É necessário
precionar a tecla 's' para dar start e 't' para entrar no tutorial (esses avisos estão na
interface do menu).

### Pause
- Existência do comando 'pause' ('p') para parar o jogo temporariamente.
Na interface pausada o usuário pode voltar ao jogo ('b'- back) ou ir para o menu ('m' - menu) e iniciar um novo jogo. 
(esses comando tambem estão especificados no tutorial).

### Inimigos
####  3 tipos de inimigos.
- 2 fracos (1 de dano e não atira se tiver uma entidade na frente ou um projetil na coluna)
    - os projeteis dos dois inimigos fracos são:
        inimigo fraco verde = shuriken verde(arma de ninja que parece uma estrela)
        inimigo fraco azul claro = um raio azul claro
- 2 moderados (2 de dano e só atira se não tiver tiro na coluna)
    - os projeteis dos dois inimigos moderados são:
        inimigo azul escuro = missel com chama azul escura
        inimigo soviétivo = missel com chama amarela e vermelha
- 1 fortes (2 de dano, atira uma vez se: tiver um projétil de um inimigo fraco ou moderado na coluna, caso não haja,
o inimigo forte atira até duas vezes na mesma coluna)
    - o projetil do inimigo forte é:
        inimigo americano = missel com chamas azul clara
- 1 nave-mãe.
    - A nave-mãe passa periódicamente na parte superior da tela e nao tem projéteis.
    - A nave-mãe tem apenas uma vida. 
- cada inimigo tem um projétil diferente.
- Os inimigos tem apenas uma vida.

### Nave 
- A nave pode atirar caso o projétil atinja um inimigo ou saia do mapa, como no jogo original.
- O projetil causa 1 de dano.
- A nave inicia com 3 vidas.
- Uma vida é removida cada vez que ela é acertada, e uma é adicionada a cada fim de rodada.

### Obstáculos
- 3 obstáculos.
- Após receber 10 de dano o obstáculo é removido.
- Os obstáculos não são iniciados novamente depois de serem removidas durante o jogo.

### Interface do jogo
- As vidas, o score e o nível de dificuldade ficam na parte superior da tela.
- Existem explosões diferentes para inimigos(aliens e nave mãe), para colisão de projéteis e
o player.
- Os pontos da nave-mãe é tabelado no score, mas também é mostrado para o jogador
quando ela é atingida, uma vez que seus pontos mudam conforme o projétil do player a atinge.

### Adicionados
- Projetéis que se anulam 
- Progressão de nível 
    - A progressão de nível consiste nos projéteis dos inimigos ficarem mais rápidos
- Nave inimiga vermelha com pontuação extra 
- Inimigos se movem em blocos para direita e esquerda 
- Implementação de 2 novos inimigos, com novo modelo de ataque e nova imagem 
    - o ataque do inimigo sovietico(moderado): ataque na diagonal esquerda
    - o ataque do inimigo americano(forte): ataque em zig-zag
    (adicional) 
    - ataque do inimigo azul escuro(moderado): ataque na diagonal direita.
- Inserção de coluna vazia entre os inimigos

## Compilação

O comando make deve ser colocado na linha de comando, assim o jogo será compilado e executado automaticamente.

## Como usar

No jogo existe uma aba "Tutorial", o qual o jogador pode acessar clicando a tecla 't'. Lá estarão as informações principais do jogo, tanto tabela de pontos quanto os comandos.

## Autor 

Nome: Gabriela Fanaia Dorst

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>
#include <allegro5/allegro_image.h>
#include <stdio.h>
#include <time.h>
#include "ship/ship.h"
#include "enemy/enemy.h"
#include "obstacle/obstacle.h"
#include "additional/additional.h"
#include "mothership/mothership.h"
#include "shoots_and_collision/shoots_and_collision.h"



int main (){
	//variaveis de controle para tiro, explosoes, niveis, frames e aparicoes.
	srand(time(NULL));
	int nivel = 1;
	int atirar[2];
	int count = 0;

	float time_mothership = 120;
	float timer_appear = 0;
	float timer_points = 0;
	int to_appear = 0;	
	int mothership_w, mothership_h;

	int timer_face = 30;
	int face = 0;
	int t = 0;

	int timer_power = 1;
	int power = 0;
	int p = 0;

	int timer_explosao = 4;
	int explosao_enemy = 0;
	int frame = 0;
	int enemy_h, enemy_w;

	int explosao2 = 0;
	int frame2 = 0;
	int collision_shoot_h, collision_shoot_w;

	int lose_life = 0;
	int explosao3 = 0;
	int frame3 =0;
	int frame4 =0;
	int ship_w, ship_h;
	int c = 0;

	int s = 0;
	int timer_zig_zag = 15;
	int shoot_zig_zag = 1;

	int game_state = MENU;
	struct ship ship;
	struct shipshoot shipshoot[2];
	struct enemy enemy[LINE][COLUMN];
	struct enemy mothership;
	struct obstacle obstacles[N_OBSTACLES];
	struct enemyshoot enemyshoot[2];

	al_init();
	must_init(al_install_keyboard(), "keyboard: ");
	must_init(al_init_font_addon(), "font: ");
	must_init(al_init_ttf_addon(), "ttf: ");
	must_init(al_init_image_addon(), "image: ");

	ALLEGRO_DISPLAY *display = al_create_display(SCREEN_W, SCREEN_H);
	al_set_window_position(display, 200, 200);
	ALLEGRO_FONT *font = al_load_font("./font/font.ttf", 25, 0);
	ALLEGRO_FONT *font_smaller = al_load_font("./font/font.ttf", 12, 0);
	ALLEGRO_TIMER* timer = al_create_timer(1.0 / 40.0);
	ALLEGRO_BITMAP *background_menu = al_load_bitmap("./sprites/MENU.png");
	ALLEGRO_BITMAP *background_tutorial = al_load_bitmap("./sprites/tutorial.png");
	ALLEGRO_BITMAP *background = al_load_bitmap("./sprites/fundo_jogo.png");
	ALLEGRO_BITMAP *player = al_load_bitmap("./sprites/player.png");
	ALLEGRO_BITMAP *enemys = al_load_bitmap("./sprites/enemys.png");
	ALLEGRO_BITMAP *sprites = al_load_bitmap("./sprites/shoots.png");
	ALLEGRO_BITMAP *mother_ship = al_load_bitmap("./sprites/mothership.png");
	ALLEGRO_BITMAP *explosion_ship = al_load_bitmap("./sprites/explosion.png");
	ALLEGRO_EVENT_QUEUE *event_queue = al_create_event_queue();
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_start_timer(timer);
	if (!display) exit(1);
	if (!font) exit(2);
	if (!font_smaller) exit(2);
	if (!timer) exit(3);
	if (!background_menu) exit(4);
	if (!background_tutorial) exit(4);
	if (!background) exit(4);
	if (!player) exit(4);
	if (!enemys) exit(4);
	if (!sprites) exit(4);
	if (!mother_ship) exit(4);
	if (!explosion_ship) exit(4);
	if (!event_queue) exit(5);

	while(true) {
		ALLEGRO_EVENT event;
		al_wait_for_event(event_queue, &event);
		if(event.type == ALLEGRO_EVENT_DISPLAY_CLOSE ){
			break;
		}

		al_clear_to_color(al_map_rgb(255,255,255));
		
		if (game_state == MENU) {

			menu(background_menu, font, font_smaller);	
			if (event.type == ALLEGRO_EVENT_KEY_DOWN){
				switch(event.keyboard.keycode) {
					case ALLEGRO_KEY_S:
						count = 0;
						timer_appear = 0;
						to_appear = 0;	
						explosao_enemy = 0;
						explosao2 = 0;
						explosao3 = 0;
						init_game(&ship, enemy, &mothership, obstacles, shipshoot, enemyshoot, &nivel, atirar);
						game_state = GAME;
						break;
					case ALLEGRO_KEY_T:
						game_state = TUTORIAL;
						break;
				}
			}

		} else if (game_state == GAME) {
			al_draw_bitmap(background, 0, 0, 0);
			timer_mothership(&timer_appear, &timer_points, &to_appear, time_mothership, &mothership);
			timer_enemy(timer_face, &face, timer_power, &power, timer_explosao, &explosao_enemy, &frame, &t, &p);
			timer_player(&lose_life, &explosao2, &frame2, &frame3, &frame4 , &c, &ship, &explosao3);
			timer_zigzag(&s, timer_zig_zag, &shoot_zig_zag);

			draw_shipshoot(shipshoot, sprites);
			draw_enemyshoot(enemyshoot, sprites, power, enemy);
			draw_ship(player, &ship);
			draw_enemy(enemys, enemy, face);
			draw_mothership(mother_ship, &mothership);
			draw_score_life(&ship, font, nivel);
			draw_obstacle(sprites, obstacles);
			draw_explosion_shoot(sprites, explosao2, collision_shoot_w, collision_shoot_h, frame2);
			draw_explosion_enemy(sprites, enemy, explosao_enemy, enemy_w, enemy_h, frame, font_smaller, &mothership);
			draw_explosion_ship(explosion_ship, &ship, explosao3, ship_w, ship_h, frame3, frame4);

			collision_enemyshoot_obstacle(obstacles, enemyshoot);
			collision_shipshoot_enemyshoot(shipshoot, enemyshoot, &ship, atirar, &explosao2, &collision_shoot_w, &collision_shoot_h);
			collision_shipshoot_enemy(shipshoot, enemy, atirar, &ship, &count, &explosao_enemy, &enemy_w, &enemy_h, &nivel);
			collision_enemyshoot_ship(&ship, enemyshoot, atirar, &explosao3, &ship_w, &ship_h, &lose_life);
			collision_shipshoot_mothership(shipshoot, &ship, &mothership, &explosao_enemy, atirar, &enemy_w, &enemy_h, &to_appear, &mothership_w, &mothership_h);
			collision_shipshoot_obstacle(shipshoot, &ship, atirar, obstacles);
			points_mothership(font_smaller, mothership_w, mothership_h, &mothership);

			update_ship(&ship);
			update_enemy(enemys, enemy);
			update_shipshoot(shipshoot, &ship, atirar);
			update_mothership(&mothership, &to_appear);
			update_enemyshoot(enemy, obstacles, &ship, enemyshoot, nivel, shoot_zig_zag);

			if (ship.life == 0) {
				explosao3 = 0;
				game_state = GAME_OVER;
			}

			if (event.type == ALLEGRO_EVENT_KEY_UP) {
				switch(event.keyboard.keycode) {
					case ALLEGRO_KEY_RIGHT:
						ship.right = 0;
						break;
					case ALLEGRO_KEY_LEFT:
						ship.left = 0;
						break;
				}
			}

			if (event.type == ALLEGRO_EVENT_KEY_DOWN){
				switch(event.keyboard.keycode) {
					case ALLEGRO_KEY_P:
						game_state = PAUSE;
						break;
					case ALLEGRO_KEY_RIGHT:
						ship.right = 1;
						break;
					case ALLEGRO_KEY_LEFT:
						ship.left = 1;
						break;
					case ALLEGRO_KEY_SPACE:
						atira(atirar);
						break;
				}
			}


		} else if (game_state == TUTORIAL) {
			tutorial(font, font_smaller, background_tutorial);

			if (event.type == ALLEGRO_EVENT_KEY_DOWN){
				switch(event.keyboard.keycode) {
					case ALLEGRO_KEY_B:
						game_state = MENU;
						break;
				}
			}
		} else if (game_state == PAUSE) {
			stop(font, font_smaller, background);

			if (event.type == ALLEGRO_EVENT_KEY_DOWN){
				switch(event.keyboard.keycode) {
					case ALLEGRO_KEY_B:
						game_state = GAME;
						break;
					case ALLEGRO_KEY_M:
						game_state = MENU;
						break;
				}
			}

		} else if (game_state == GAME_OVER) {
			game_over(background, font, font_smaller);

			if (event.type == ALLEGRO_EVENT_KEY_DOWN){
				switch(event.keyboard.keycode) {
					case ALLEGRO_KEY_M:
						game_state = MENU;
						break;
				}
			}

		} 

		al_flip_display();
	}

	al_destroy_bitmap(background_menu);
	al_destroy_bitmap(background_tutorial);
	al_destroy_bitmap(background);
	al_destroy_bitmap(player);
	al_destroy_bitmap(explosion_ship);
	al_destroy_bitmap(sprites);
	al_destroy_bitmap(enemys);
	al_destroy_bitmap(mother_ship);
	al_destroy_font(font);
	al_destroy_font(font_smaller);
	al_destroy_display(display);
	al_destroy_event_queue(event_queue);

	return 0;
}

#ifndef _SHOOTSANDCOLLISION_
#define _SHOOTSANDCOLLISION_

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include "../enemy/enemy.h"
#include "../ship/ship.h"
#include "../mothership/mothership.h"
#include "../obstacle/obstacle.h"

#define SCREEN_W 960
#define SCREEN_H 540

struct shipshoot {
	float center_w, center_h;
};

struct enemyshoot {
	int center_w, center_h;
	int i, j;
	int speed;
	int damage;
};

void init_enemyshoot(struct enemyshoot *enemyshoot);
void init_ship_shoot(struct shipshoot *shoot, struct ship *ship);
void init_atirar(int *atirar);
void atira(int *atirar);
void update_shipshoot(struct shipshoot *shoot, struct ship *ship, int *atirar);
void update_enemyshoot(struct enemy enemy[][COLUMN], struct obstacle *obstacle,
		struct ship *ship,struct enemyshoot *enemyshoot, int nivel, int zigzag);
void collision_shipshoot_obstacle(struct shipshoot *shoot, struct ship *ship, 
		int *atirar, struct obstacle *obstacle);
void collision_shipshoot_mothership(struct shipshoot *shoot, struct ship *ship, 
		struct enemy *mothership, int *explosao, int *atirar, int *w, int *h, int *to_appear, int *m_w, int *m_h);
void collision_shipshoot_enemy(struct shipshoot *shoot, struct enemy enemy[][COLUMN], 
		int *atirar,struct ship *ship, int *count, int *explosao, int *w, int *h, int *nivel);
void collision_shipshoot_enemyshoot(struct shipshoot *shipshoot, struct enemyshoot *enemyshoot,
		struct ship *ship, int *atirar, int *explosao, int *w, int *h);
void collision_enemyshoot_ship(struct ship * ship, struct enemyshoot *enemyshoot, 
		int *atirar, int *explosao, int *w, int *h, int *lose_life);
void collision_enemyshoot_obstacle(struct obstacle *obstacle, struct enemyshoot *enemyshoot);
void permission_to_shoot(struct enemy enemy[][COLUMN], struct enemyshoot *enemyshoot, int shoot, int i, int j);
void draw_enemyshoot(struct enemyshoot *enemyshoot, ALLEGRO_BITMAP *shoot, int frame, struct enemy enemy[][COLUMN]);
void draw_shipshoot(struct shipshoot *ship_shoot, ALLEGRO_BITMAP *shoot);
void draw_explosion_shoot(ALLEGRO_BITMAP *explosion, int explosao, int w, int h, int frame);
void timer_zigzag(int *s, int timerzigzag, int *shoot_zig_zag);

#endif

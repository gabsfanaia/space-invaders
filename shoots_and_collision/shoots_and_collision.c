#include "shoots_and_collision.h"

#define MAX_OBSTACLES 3
#define MAX_SHIPSHOOT 2
#define MAX_ENEMYSHOOT 2

/*init_enemyshoot - inicializia os projeteis dos inimigos*/
void init_enemyshoot(struct enemyshoot *enemyshoot) {
	for (int i = 0; i < MAX_ENEMYSHOOT; i++) {
		enemyshoot[i].center_w = -40;
		enemyshoot[i].center_h = -40;
		//velocidade inicial dos projeteis
		enemyshoot[i].speed = 2;
		enemyshoot[i].i = -1;
		enemyshoot[i].j = -1;
		enemyshoot[i].damage = 0;
	}
}

/*init_ship_shoot - inicializa os projeteis da nave do player*/
void init_ship_shoot(struct shipshoot *shoot, struct ship *ship) {	
	for (int i = 0; i < MAX_SHIPSHOOT; i++) {
		shoot[i].center_w = ship->center_w;
		shoot[i].center_h = ship->center_h+10;
	}
}

/*init_atirar - inicializa as variaveis que controlam os tiros do player*/
void init_atirar(int *atirar) {
	for (int i = 0; i < MAX_SHIPSHOOT; i++) {
		atirar[i] = 0;
	}
}

/*atira - guarda comando de tiro(quando o player aperta espaco) em um vetor para sinalizar pedido*/
void atira(int *atirar) {
	//contabiliza o numero de tiros dados pelo player
	for (int i = 0; i < MAX_SHIPSHOOT; i++) {
		if (atirar[i] == 0) {
			atirar[i] = 1;
			return;
		}
	}
}

/*collision_shipshoot_obstacle - verifica se ha colisao entre o projetil da nave e o obstaculo*/
void collision_shipshoot_obstacle(struct shipshoot *shoot, struct ship *ship, int *atirar, 
		struct obstacle *obstacle) {
	for (int i = 0; i < MAX_OBSTACLES; i++) {
		for (int j = 0; j < MAX_SHIPSHOOT; j ++) {
			//verica se o projetil esta no intervalo do obstaculo
			if (shoot[j].center_w+35 <= obstacle[i].center_w+120 && shoot[j].center_w+35 >= obstacle[i].center_w+5 && 
					shoot[j].center_h <= obstacle[i].center_h+45 && shoot[j].center_h >= obstacle[i].center_h-55) {
				atirar[j] = 0;
				obstacle[i].damage--;
				//computa o dano e guarda a mudanca de frame
				if (obstacle[i].damage <= 0) {
					obstacle[i].f = 0;
					obstacle[i].center_h = 600;
				} else if (obstacle[i].damage <= 2) {
					obstacle[i].f = 4;
				} else if(obstacle[i].damage <= 4) {
					obstacle[i].f = 3;
				} else if(obstacle[i].damage <= 6) {
					obstacle[i].f = 2;
				} else if (obstacle[i].damage <= 8) {
					obstacle[i].f = 1;
				}
				return;
			}
		}
	}
}

/*collision_shipshoot_mothership - verifica se ha colisao entre o projetil do player e a nave-mae*/
void collision_shipshoot_mothership(struct shipshoot *shoot, struct ship *ship, struct enemy *mothership, 
		int *explosao, int *atirar, int *w, int *h, int *to_appear, int *m_w, int *m_h) {
	for (int i = 0; i < MAX_SHIPSHOOT; i++) {
		//verica se o projetil esta no intervalo da nave-mae
		if (shoot[i].center_w+35 <= mothership->center_w+35 && shoot[i].center_w+35 >= mothership->center_w-35 &&
				shoot[i].center_h <= mothership->center_h+20 && shoot[i].center_h >= mothership->center_h-20) {
			atirar[i] = 0;
			//guarda posicao para o frame da explosao
			*w = mothership->center_w;
			*h = mothership->center_h;
			//guarda a posicao para o time dos pontos
			*m_w = *w;
			*m_h = *h;
			mothership->center_w = SCREEN_W + 50;
			ship->score += mothership->points;
			mothership->points += 50;
			mothership->life = 0;

			*explosao = 1;
			*to_appear = 0;

		}
	}
}

/*collision_shiphoot_enemy - verifica se ha colisao entre o projetil do player e o inimigo */
void collision_shipshoot_enemy(struct shipshoot *shoot, struct enemy enemy[][COLUMN], int *atirar,
		struct ship *ship, int *count, int *explosao, int *w, int *h, int *nivel) {
	for (int i = 0; i < LINE; i++) {
		for (int j = 0; j < COLUMN; j++) {
			for (int k = 0; k < MAX_SHIPSHOOT; k++) {
			//verica se o projetil esta no intervalo do inimigo
				if (shoot[k].center_w+35 <= enemy[i][j].center_w+28 && shoot[k].center_w+35 >= enemy[i][j].center_w-18 &&
						shoot[k].center_h <= enemy[i][j].center_h && shoot[k].center_h >=enemy[i][j].center_h-20) {
					atirar[k] = 0;
					//guarda os pontos da colisao
					*w = enemy[i][j].center_w;
					*h = enemy[i][j].center_h;
					enemy[i][j].center_h = SCREEN_H + 40;
					enemy[i][j].life = 0;
					ship->score += enemy[i][j].points;
					*count += 1;
					*explosao = 1;

					if (*count == 50) {
						*count = 0;
						shoot[k].center_h = ship->center_h;
						shoot[k].center_w = ship->center_w;
						init_enemy(enemy);
						ship->life++;
						*nivel += 1;
					}
				}
			}
		}
	}
}

/*collision_shipshoot_enemyshoot -- verifica se ha colilsao entre os projeteis do player e do inimigo*/
void collision_shipshoot_enemyshoot(struct shipshoot *shipshoot, struct enemyshoot *enemyshoot,
		struct ship *ship, int *atirar, int *explosao, int *w, int *h){ 
	for (int i = 0; i < MAX_ENEMYSHOOT;  i++) {
		for (int j = 0; j < MAX_SHIPSHOOT; j++) {
			//verica se o projetil esta no intervalo do projetil do inimigo
			if (enemyshoot[i].center_w+5 <= shipshoot[j].center_w+45 && enemyshoot[i].center_w+5 >= shipshoot[j].center_w+25 &&
					shipshoot[j].center_h <= enemyshoot[i].center_h+15 && enemyshoot[i].center_h <= shipshoot[j].center_h+10) {
				atirar[j] = 0;
				*explosao = 1;
				//guarda os pontos da colisao
				*w = enemyshoot[i].center_w;
				*h = enemyshoot[i].center_h;
				enemyshoot[i].center_w = -1000;
				enemyshoot[i].center_h = -40;
				shipshoot[j].center_w = ship->center_w;
				shipshoot[j].center_h = ship->center_h;
			}
		}
	}
}

/*update-shishoot - atualiza os tiros que estao no mapa e verifica permissao para atirar na coluna*/
void update_shipshoot(struct shipshoot *shoot, struct ship *ship, int *atirar) {
	//verifica se nao ha projetil na coluna
	for (int i = 0; i < MAX_SHIPSHOOT; i++) {
		for (int j = 0; j < MAX_SHIPSHOOT; j++) {
			if (i!=j && shoot[i].center_h == shoot[j].center_h) {
				continue;
			} else if (i != j && atirar[j] == 1 && shoot[j].center_h == ship->center_h+10 && shoot[i].center_h != ship->center_h && (shoot[j].center_w >= shoot[i].center_w-80 && shoot[j].center_w <= shoot[i].center_w+80)) {
				atirar[j] = 0;
			}
		}
	}

	//atualiza o projetil se tiver dentro das regras para atirar
	for (int i = 0; i < MAX_SHIPSHOOT; i++) {
		if (atirar[i] == 1 && shoot[i].center_h > - 20) {
			shoot[i].center_h -= 15;
		} else {
			atirar[i] = 0;
			shoot[i].center_w = ship->center_w;
			shoot[i].center_h = ship->center_h + 10;
		}

	}
}

/*collision_enemyshoot_ship - verifica se o projetil do inimigo acerta a nave do player*/
void collision_enemyshoot_ship(struct ship * ship, struct enemyshoot *enemyshoot, int *atirar, int *explosao, 
		int *w, int *h, int *lose_life) {
	for (int i = 0; i < MAX_ENEMYSHOOT;  i++) {
		//verifica se o projetil esta no mesmo intervalo da nave do player
		if (enemyshoot[i].center_w+5 <= ship->center_w+70 && enemyshoot[i].center_w+5 >= ship->center_w &&
			ship->center_h <= enemyshoot[i].center_h+10) {
				*atirar = 0;
				*explosao = 1;
				//guarda os pontos da colisao
				*w = ship->center_w;
				*h = ship->center_h;
				enemyshoot[i].center_w = -1000;
				enemyshoot[i].center_h = -40;
				ship->center_w = SCREEN_W + 50;
				ship->center_h = SCREEN_H + 50;
				*lose_life = 1;
				ship->life--;

		}
	}
}

/*collision_enemyy_obstacles - verifica se o projetil do inimigo atinge o obstaculo*/
void collision_enemyshoot_obstacle(struct obstacle *obstacle, struct enemyshoot *enemyshoot) {
	for (int i = 0; i < MAX_OBSTACLES; i++) {
		for (int j = 0; j < MAX_ENEMYSHOOT;  j++) {
			//verica se o projetil esta no intervalo do obstaculo
			if (enemyshoot[j].center_w <= obstacle[i].center_w+110 && enemyshoot[j].center_w>= obstacle[i].center_w-5 &&
			enemyshoot[j].center_h+20 <= obstacle[i].center_h+100 && enemyshoot[j].center_h+20 >= obstacle[i].center_h+50)  {
					enemyshoot[j].center_w = -1000;
					enemyshoot[j].center_h = -40;	
					obstacle[i].damage -= enemyshoot[j].damage;

					//computa o dano e muda o frame do obstaculo
					if (obstacle[i].damage <= 0) {
						obstacle[i].f = 0;
						obstacle[i].center_h = 600;
					} else if (obstacle[i].damage <= 2) {
						obstacle[i].f = 4;
					} else if(obstacle[i].damage <= 4) {
						obstacle[i].f = 3;
					} else if(obstacle[i].damage <= 6) {
						obstacle[i].f = 2;
					} else if (obstacle[i].damage <= 8) {
						obstacle[i].f = 1;
				}
			}
		}
	}
}

/*ppermission_to_shoot - verifica se os inimigos estao cumprindo todas condicoes necessarias para atirar*/
void permission_to_shoot(struct enemy enemy[][COLUMN], struct enemyshoot *enemyshoot,
		int n, int i, int j) {
	int noti = !n;
	//Verifica se o mesmo inimigo foi escolhido na mesma rodada para dar o tiro. Se nao for
	//um inimigo forte, o ultimo tiro escolhido e descartado.
	if (enemyshoot[0].i == enemyshoot[1].i && enemyshoot[0].j == enemyshoot[1].j &&
			enemy[enemyshoot[n].i][enemyshoot[n].j].points != 40) {
		enemyshoot[n].center_w = -1000;
		enemyshoot[n].center_h = -40;
	} else if (enemyshoot[n].center_w+5 <= enemyshoot[noti].center_w+60 &&
			enemyshoot[n].center_w+5 >= enemyshoot[noti].center_w-60 &&
			enemy[enemyshoot[n].i][enemyshoot[n].j].points != 40) {
	//Verifica se os projeteis estao no mesmo intervalo(coluna). Se tiverem no mesmo intervalo(coluna) e o inimigo
	//nao for forte, o ultimo tiro e descartado.
		enemyshoot[n].center_w = -1000;
		enemyshoot[n].center_h = -40;
	} else if (i == 3 && enemy[i+1][j].life == 1) {
	//Verifica se o inimigo escolhido na rodada e fraco. Caso for fraco e houver outro inimigo fraco na frente
	//o ultimo tiro e descartado.
		enemyshoot[n].center_w = -1000;
		enemyshoot[n].center_h = -40;
	}
}

/*uppdate_enemyshoot - escolhe os inimigos q irao atirar na rodada e atualiza os tiros*/
void update_enemyshoot(struct enemy enemy[][COLUMN], struct obstacle *obstacle, struct ship *ship,
		struct enemyshoot *enemyshoot, int nivel, int zigzag) {
	int random = rand() % 10; 

	//move os projeteis de acordo com cada tipo de ataque		
	for (int n = 0; n < MAX_ENEMYSHOOT; n++) {
		if (enemyshoot[n].center_h > SCREEN_H) {
			enemyshoot[n].center_h = -40;
			enemyshoot[n].center_w = -1000;
		} else if (enemyshoot[n].i == 4 || enemyshoot[n].i == 3 ) {
			enemyshoot[n].center_h += (nivel * 2);
		}else if (enemyshoot[n].i == 2) {
			enemyshoot[n].center_h += (nivel * 2);
			enemyshoot[n].center_w += (nivel * 2);
		}  else if (enemyshoot[n].i == 1) {
			enemyshoot[n].center_h += (nivel * 2);
			enemyshoot[n].center_w += (nivel * -1 * 2);
		} else if (enemyshoot[n].i == 0) {
			enemyshoot[n].center_h += (nivel * 2);
			enemyshoot[n].center_w += (nivel * 2 * zigzag);
		}
	}
	//escolhe os inimigos que irao atirar na rodada
	for (int i = 0; i < LINE; i++) {
		//inimigo escolhido aleatoriamente iniciando da ultima linha
		if (enemy[4-i][random].life == 1 && enemyshoot[1].center_h <=0) {
			enemyshoot[1].center_w = enemy[4-i][random].center_w;
			enemyshoot[1].center_h = enemy[4-i][random].center_h;
			enemyshoot[1].i = 4-i;
			enemyshoot[1].j = random;
			enemyshoot[1].damage = enemy[4-i][random].damage;
			permission_to_shoot(enemy, enemyshoot, 1, 4-i, random);
		}
		for (int j = 0; j < COLUMN; j++) {
			//inimigo escolhido na mesma coluna que o player, preferencialmente os inimigos mais proximos
			if (enemy[4-i][j].life == 1 && enemyshoot[0].center_h <= 0 && 
			enemy[4-i][j].center_w-20 <= ship->center_w+30 && enemy[4-i][j].center_w+20 >= ship->center_w-30) {
					enemyshoot[0].center_w = enemy[4-i][j].center_w;
					enemyshoot[0].center_h = enemy[4-i][j].center_h;
					enemyshoot[0].i = 4-i;
					enemyshoot[0].j = j;
					enemyshoot[0].damage = enemy[4-i][j].damage;
					permission_to_shoot(enemy, enemyshoot, 0, 4-i, j);
			}
		}
	}
}

void draw_enemyshoot(struct enemyshoot *enemyshoot, ALLEGRO_BITMAP *shoot, int frame,
		struct enemy enemy[][COLUMN]) {
	//verifica cada inimigo para imprimir seu projetil
	for (int i = 0; i < MAX_ENEMYSHOOT; i++) {
		if (enemyshoot[i].i == 4) {
			al_draw_bitmap_region(shoot, 0 + frame * 20, 225, 20, 20, enemyshoot[i].center_w, enemyshoot[i].center_h, 0);
		} else if (enemyshoot[i].i == 3) {
			al_draw_bitmap_region(shoot, 0 + frame * 20, 268, 20, 20, enemyshoot[i].center_w, enemyshoot[i].center_h, 0);
		} else if (enemyshoot[i].i == 2) {
			al_draw_bitmap_region(shoot, 0 + frame * 20, 245, 20, 20, enemyshoot[i].center_w, enemyshoot[i].center_h, 0);
		} else if (enemyshoot[i].i == 1) {
			al_draw_bitmap_region(shoot, 0 + frame * 20, 290, 20, 30, enemyshoot[i].center_w, enemyshoot[i].center_h, 0);
		} else  {
			al_draw_bitmap_region(shoot, 0 + frame * 20, 320, 20, 30, enemyshoot[i].center_w, enemyshoot[i].center_h - 15, 0);
		} 
	}
}

void draw_shipshoot(struct shipshoot *ship_shoot, ALLEGRO_BITMAP *shoot) {
	for (int i = 0; i < MAX_SHIPSHOOT; i++) {
		al_draw_bitmap_region(shoot, 0, 0, 70, 10, ship_shoot[i].center_w, ship_shoot[i].center_h, 0);
	}
}

void draw_explosion_shoot(ALLEGRO_BITMAP *explosion, int explosao, int w, int h, int frame) {
	if (explosao)
		al_draw_bitmap_region(explosion, 0 + frame * 60, 390, 60, 40, w - 25, h - 20, 0);
}

void timer_zigzag(int *s, int timerzigzag, int *shoot_zig_zag) {
	*s += 1;
	//controla o projetil do inimigo forte 
		if (*s > timerzigzag) {
			*s = 0;
			if (*shoot_zig_zag == -1) {
				*shoot_zig_zag = 1;
			} else {
				*shoot_zig_zag = -1;
			}
		}
}

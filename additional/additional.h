#ifndef _ADDITIONAL_
#define _ADDITIONAL_

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <stdio.h>
#include "../enemy/enemy.h"
#include "../ship/ship.h"
#include "../obstacle/obstacle.h"
#include "../mothership/mothership.h"
#include "../shoots_and_collision/shoots_and_collision.h"

enum GameState {
	MENU, GAME, TUTORIAL, PAUSE, GAME_OVER
};

void init_game(struct ship *ship, struct enemy enemy[][COLUMN], struct enemy *mothership, struct obstacle *obstacles, 
		struct shipshoot *shipshoot, struct enemyshoot *enemyshoot, int *nivel, int *atirar);
void draw_score_life (struct ship *ship, ALLEGRO_FONT *font, int nivel);
void menu(ALLEGRO_BITMAP *background_menu, ALLEGRO_FONT *font, ALLEGRO_FONT *font_smaller);	
void tutorial(ALLEGRO_FONT *font, ALLEGRO_FONT *font_smaller, ALLEGRO_BITMAP *background_tutorial);
void stop(ALLEGRO_FONT *font, ALLEGRO_FONT *font_smaller, ALLEGRO_BITMAP *background);
void game_over(ALLEGRO_BITMAP *background, ALLEGRO_FONT *font, ALLEGRO_FONT *font_smaller);
void must_init(bool test, const char *description);
void init_atirar(int *atirar);

#endif

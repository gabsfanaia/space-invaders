#include "additional.h"

/*draw_score_life - imprimi o score e a quantidade de vida na interface
 * @ship - ponteiro para struct ship
 * @font - ponteiro para fonte escolhida
 * @nivel - inteiro que indica o nivel atual do jogo*/
void draw_score_life (struct ship *ship, ALLEGRO_FONT *font, int nivel) {
	char score[500];
	char lifes[100];
	char niveis[100];
	sprintf(score, "SCORE: %d", ship->score);
	sprintf(lifes, "LIFES: %d", ship->life);
	sprintf(niveis, "NIVEL: %d", nivel);
	al_draw_text(font, al_map_rgb(173, 216, 230), 0, 0, 0, score);	
	al_draw_text(font, al_map_rgb(173, 216, 230), 790, 0, 0, lifes);	
	al_draw_text(font, al_map_rgb(173, 216, 230), 440, 0, 0, niveis);	
}

/* must_init - verifica se as funções inicializaram corretamente
 * @teste - booleano que verifica o retorno da funcao
 * @description - ponteiro para char com nome da funcção inicializada
 * */
void must_init(bool test, const char *description)
{
	if(test) return;

	printf("couldn't initialize %s\n", description);
	exit(1);
}

/* menu - imprimi o menu do jogo
 * @background_menu - ponteiro para a imagem de fundo do menu
 * @font - ponteiro para a fonte escolhida
 * @font_smaller - ponteiro para fonte escolhida
 * */
void menu(ALLEGRO_BITMAP *background_menu, ALLEGRO_FONT *font, ALLEGRO_FONT *font_smaller) {
	al_draw_bitmap(background_menu, 0, 0, 0);
	al_draw_text(font, al_map_rgb(173, 216, 230), 350, 275, 0, "Start");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 550, 290, 0, "(press s)");	
	al_draw_text(font, al_map_rgb(173, 216, 230), 350, 335, 0, "Tutorial");
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 550, 350, 0, "(press t)");


}

/* tutorial - imprimi o tutorial do jogo
 * @background_tutorial - ponteiro para a imagem de fundo do tutorial
 * @font - ponteiro para a fonte escolhida
 * @font_smaller - ponteiro para fonte escolhida
 * */
void tutorial(ALLEGRO_FONT *font, ALLEGRO_FONT *font_smaller, ALLEGRO_BITMAP *background_tutorial) {	
	al_draw_bitmap(background_tutorial, 0, 0, 0);
	al_draw_text(font, al_map_rgb(173, 216, 230), 0, 30, 0, "  Score advance table         Comands");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 130, 100, 0, "10 points               damage: 1");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 130, 170, 0, "10 points               damage: 1");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 130, 250, 0, "20 points               damage: 2");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 130, 330, 0, "20 points               damage: 2");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 130, 410, 0, "40 points               damage: 2");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 160, 480, 0, "?? Mistery");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 470, 100, 0, "- Press 'space' to shoot");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 470, 170, 0, "- Press 'arrow left' and 'arrow right' to move the ship");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 470, 250, 0, "- Press 'p' to pause");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 470, 330, 0, "- Press 'b' to come back");	


}

/* stop - imprimi o pause do jogo
 * @background - ponteiro para a imagem de fundo do jogo
 * @font - ponteiro para a fonte escolhida
 * @font_smaller - ponteiro para fonte escolhida
 * */
void stop(ALLEGRO_FONT *font, ALLEGRO_FONT *font_smaller, ALLEGRO_BITMAP *background) {
	al_draw_bitmap(background, 0, 0, 0);
	al_draw_text(font, al_map_rgb(173, 216, 230), 410, 100, 0, "PAUSE");
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 350, 200, 0, "- Press 'b' to back to the game");	
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 350, 250, 0, "- Press 'm' to go to the game");	
}

/* game over- imprimi o final do jogo
 * @background - ponteiro para a imagem de fundo do jogo
 * @font - ponteiro para a fonte escolhida
 * @font_smaller - ponteiro para fonte escolhida
 * */
void game_over(ALLEGRO_BITMAP *background, ALLEGRO_FONT *font, ALLEGRO_FONT *font_smaller) {
	al_draw_bitmap(background, 0, 0, 0);
	al_draw_text(font, al_map_rgb(173, 216, 230), 410, 100, 0, "GAME OVER");
	al_draw_text(font_smaller, al_map_rgb(255, 255, 255), 350, 200, 0, "- Press 'm' to back to the menu");	
}

/* init_game - inicializa principais estruturas do jogo;
 * @nivel - inteiro que inicializa o nivel
 * @atirar - ponteiro para um vetor de inteiros
 * */
void init_game(struct ship *ship, struct enemy enemy[][COLUMN], struct enemy *mothership,
		struct obstacle *obstacles, struct shipshoot *shipshoot, struct enemyshoot *enemyshoot,
		int *nivel, int *atirar) {
	init_ship(ship);
	init_enemy(enemy);
	init_mothership(mothership);
	init_obstacle(obstacles);
	init_ship_shoot(shipshoot, ship);
	init_enemyshoot(enemyshoot);
	atira(atirar);
	*nivel = 1;
}

#include "ship.h"

#define SCREEN_W 960
#define SCREEN_H 540

/*init_ship - inicializa a nave do player com seu ponto no mapa, velocidade, vida
 * e score*/
void init_ship(struct ship *ship) {
	ship->center_w = (SCREEN_W / 2) - 35;
	ship->center_h = SCREEN_H - 30;
	//velocidade do projetil
	ship->speed = 15;
	ship->right = 0;
	ship->left = 0;
	ship->life = 3;
	ship->score = 0;

}

/*update_ship - atualiza a posição do player*/
void update_ship(struct ship *ship) {
	int position_right = ship->center_w + 80 + ship->speed;
	int position_left = ship->center_w - 35 + ship->speed;
	//move a nave para direita e esquerda
	if (ship->right && position_right <= SCREEN_W) {
		ship->center_w += ship->speed;
	} else if (ship->left && position_left >= 0) {
		ship->center_w -= ship->speed;
	}
}
/*draw_ship - imprimi a nave do player no mapa*/
void draw_ship (ALLEGRO_BITMAP *player, struct ship *ship) {
	al_draw_bitmap_region(player, 0, 0, 70, 35, ship->center_w, ship->center_h, 0);
}

/*draw_explosion_ship - imprimi a explosao do player
 * @w, @h - inteiros que guardam a posicao onde a nave foi atingida
 * @explosao - inteiro que guarda se houve ou nao explosao da nave
 * @frame3, frame4 - inteiros que guardam os frames da explosao
 * */
void draw_explosion_ship(ALLEGRO_BITMAP *explosion, struct ship *ship, int explosao, int w, int h, int frame3, int frame4) {
	if (explosao)
		al_draw_bitmap_region(explosion, 0 + frame3 * 170, 0 + frame4 * 130 , 170, 130, w-40, h-90, 0);
}

/* timer_player - computa o tempo para controle de frames
 * @explosao2 - ponteiro para inteiro que controla a explosao dos tiros entre si
 * @lose_life - ponteiro para inteiro que guarda se a nave do player perdeu vida
 * @frame2, @frame3, @frame4 - ponteiro para inteiro que controla os frames das explosoes
 * */
void timer_player(int *lose_life, int *explosao2, int *frame2, int *frame3, int *frame4, 
		int *count_frames, struct ship *ship, int *explosao3) {
	//controla o frame da explosao dos tiros entre si
	if (*explosao2  && *frame2 < 8) {
		*frame2 += 1;
	} else {
		*frame2 = 0;
		*explosao2 = 0;
	}

	//controla o frame da explosao do player
	if (*lose_life && *count_frames < 22) {
		*count_frames += 1;
		*frame3 += 1;
		if (*frame3 % 5 == 0) {
			*frame4 += 1;
			*frame3 = 0;
		}
	} else if (*count_frames == 22) {
		*lose_life = 0;
		ship->center_w = (SCREEN_W / 2) - 35;
		ship->center_h = SCREEN_H - 30;
		*frame3 = 0;
		*frame4 = 0;
		*count_frames = 0;
		*explosao3 = 0;

	}

}

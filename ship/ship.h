#ifndef _SHIP_
#define _SHIP_

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

struct ship {
	double center_w, center_h;
	int speed;
	int right, left;
	int life;
	int score;
};

void init_ship(struct ship *ship);
void update_ship(struct ship *ship);
void draw_ship (ALLEGRO_BITMAP *player, struct ship *ship);
void draw_explosion_ship(ALLEGRO_BITMAP *explosion, struct ship *ship, int explosao, int w, int h, int frame3, int frame4);
void timer_player(int *lose_life, int *explosion, int *f1, int *f2, int *f3, int *count_frames, struct ship *ship, int *explosion2);

#endif

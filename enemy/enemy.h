#ifndef _ENEMY_
#define _ENEMY_

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>


#define LINE 5
#define COLUMN 10

struct enemy {
	double center_w, center_h;
	int w_speed;
	int direction;
	int points;
	int life;
	int damage;
};

void init_enemy(struct enemy enemy[][COLUMN]);
void define_points_enemy(struct enemy enemy[][COLUMN], int i, int j);
void update_enemy(ALLEGRO_BITMAP *enemys, struct enemy enemy[][COLUMN]);
void draw_enemy(ALLEGRO_BITMAP *enemys, struct enemy enemy[][COLUMN], int face);
void draw_explosion_enemy(ALLEGRO_BITMAP *explosion, struct enemy enemy[][COLUMN], int explosao, int w, int h, int frame,ALLEGRO_FONT *font_smaller, struct enemy *mothership);
void timer_enemy(int time_face, int *face, int time_power, int *power, int time_explosao, int *explosao, int *frame, int *t, int *p);

#endif

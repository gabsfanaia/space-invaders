#include "enemy.h"

#define SCREEN_W 960
#define SCREEN_H 540

/*define_points_enemy - define a pontuação dos inimigos ao serem atingidos*/
void define_points_enemy(struct enemy enemy[][COLUMN], int i, int j) {
	if (i == 0) {
		enemy[i][j].points = 40;
		enemy[i][j].damage = 2;
	} else if (i == 1 || i == 2) {
		enemy[i][j].points = 20;
		enemy[i][j].damage = 2;
	} else if (i == 3 || i == 4) {
		enemy[i][j].points = 10;
		enemy[i][j].damage = 1;
	} 
}

/*init_enemy - inicializa a estrutura struct de cada inimigo com os pontos, dano,
 * velocidade e vida*/
void init_enemy(struct enemy enemy[][COLUMN]) {
	int w = 5;
	int h = 120;
	for (int i = 0; i < LINE; i++) {
		for (int j = 0; j < COLUMN; j++) {
			//adicao de colunas entre os inimigos
			if (j == 3) {
				w += 50;
			}

			if (j == 7) {
				w += 50;
			}
			enemy[i][j].center_w = w;
			enemy[i][j].center_h = h;
			//velocidade do projetil
			enemy[i][j].w_speed = 1;
			enemy[i][j].direction = 0;
			enemy[i][j].life = 1;
			define_points_enemy(enemy, i, j);
			w += 60;
		}
		w = 5;
		h += 40;
	}
}

/*update_enemy - atualiza a posição do inimigo no mapa*/
void update_enemy(ALLEGRO_BITMAP *enemys, struct enemy enemy[][COLUMN]) {
	for (int i = 0; i < LINE; i++) {
		for (int j = 0; j < COLUMN; j++) {
			//move os inimigs para direita e para esquerda
			if (enemy[LINE-1][COLUMN-1].center_w+25 < SCREEN_W && enemy[LINE-1][COLUMN-1].direction == 0){
				enemy[i][j].center_w++;
			} else if (enemy[0][0].center_w - 15 > 0) {
				enemy[i][j].center_w--;
				enemy[i][j].direction = 1;
			} else {
				enemy[i][j].direction = 0;
			}
		}

	}
}

/*draw_enemy - imprimi os inimigos na tela
 * @face - inteiro que alterna a imagem dos inimigos*/
void draw_enemy(ALLEGRO_BITMAP *enemys, struct enemy enemy[][COLUMN], int face) {
	int w = 0;
	int h = 162;
	for (int i = 0; i < LINE; i++) {
		for (int j = 0; j < COLUMN; j++) {
			al_draw_bitmap_region(enemys, w + face * 60, h, 60, 40, 
					enemy[i][j].center_w - 25 , enemy[i][j].center_h - 20, 0);
		}
		h -= 40;
	}
}

/* draw_explosion_enemy - imprimi a explosao dos inimigos
 * @frame - inteiro que controla o frame da explosao
 * @explosao - inteiro que verifica se há explosao
 * @w, @h - guardam os pontos onde o inimigo foi atingido
 * */
void draw_explosion_enemy(ALLEGRO_BITMAP *explosion, struct enemy enemy[][COLUMN], int explosao, int w, int h, int frame,
		ALLEGRO_FONT *font_smaller, struct enemy *mothership) {
	if (explosao)
		al_draw_bitmap_region(explosion, 0 + frame * 60, 50, 60, 70, (w - 30) + frame * 2, h - 40, 0);
}

/* timer_enemy - controla o timer das ações inimigas
 * @time_face, @time_power - inteiro que contem o tempo min de um frame
 * @t, @p - inteiros que controlam as imagens alternantes
 * @explosao - inteiro que verifica se há explosao
 * @frame - inteiro que controla a quantidade de frames da explosao dos inimigos
*/
void timer_enemy(int time_face, int *face, int time_power, int *power, int time_explosao,
		int *explosao, int *frame, int *t, int *p) {
	//timer para as imagens dos inimigos alternarem
	*t += 1;
	if (*t > time_face) {
		*t = 0;
		*face = !(*face);
	}

	//timer para as imagens dos tiros dos inimigos alternarem
	*p += 1;
	if (*p > time_power) {
		*p = 0;
		*power = !(*power);
	}

	//timer para controlar o frame da explosao dos inimigos
	if (*explosao  && *frame < 8) {
		*frame += 1;
	} else {
		*frame = 0;
		*explosao = 0;
	}
}

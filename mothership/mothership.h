#ifndef _MOTHERSHIP_
#define _MOTHERSHIP_

#include <allegro5/allegro.h>
#include <allegro5/allegro_font.h>
#include <stdio.h>
#include "../enemy/enemy.h"
#include "../ship/ship.h"

void init_mothership(struct enemy *mothership);
void update_mothership(struct enemy *mothership, int *to_appear);
void draw_mothership (ALLEGRO_BITMAP *enemys, struct enemy *mothership);
void points_mothership(ALLEGRO_FONT *font_smaller, int w, int h, struct enemy *mothership);
void timer_mothership(float *timer_appear, float *timer_points, int *to_appear, int time_mothership, struct enemy *mothership);

#endif

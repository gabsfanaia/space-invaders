#include "mothership.h"

/*init_mothership - inicializa a nave-mae com seus pontos no mapa, velocidade, pontuação
 * e vida*/
void init_mothership(struct enemy *mothership) {
	mothership->center_w = 970;
	mothership->center_h = 40;
	mothership->w_speed = 1;
	mothership->direction = 0;
	mothership->points = 100;
	mothership->life = 1;
}

/*update_mothership - atualiza o estado atual da nave-mae
 * @to_appear - ponteiro para inteiro que guarda se há ou não aparicao da nave-mae
 * */
void update_mothership(struct enemy *mothership, int *to_appear) {
	if (*to_appear && mothership->center_w > -75) {
		mothership->center_w -= mothership->w_speed;
	} else {
		mothership->center_w = 1110;
	}
}

/*draw_mothership - imprimi a nave mãe no mapa*/
void draw_mothership (ALLEGRO_BITMAP *enemys, struct enemy *mothership) {
	al_draw_bitmap_region(enemys, 0, 0, 75, 35, mothership->center_w - 35, mothership->center_h, 0);
}

/*points_mothership - imprimi os pontos quando a nave mae e atingida
 * @w, @h- inteiros que guardam os pontos onde o score da nave aparecerao
 * */
void points_mothership(ALLEGRO_FONT *font_smaller, int w, int h, struct enemy *mothership) {
	char points[50];
	if (mothership->life == 0) {
		sprintf(points, "%d++", mothership->points);
		al_draw_text(font_smaller, al_map_rgb(255, 255, 255), w-10, h+20, 0, points);	
	} 

}

/* timer_mothership - imprimi os pontos quando a nave mae e atingida
 * @time_mothership - ponteiro para inteiro que contem o intervalo de aparicao da nave-mae
 * @to_appear - ponteiro para inteiro que controla a aparicao da nave-mae
 * @timer_points - controla o tempo de aparicao dos pontos da nave-mae
 * */
void timer_mothership(float *timer_appear, float *timer_points, int *to_appear, int time_mothership, struct enemy *mothership) {	
	//timer para a nave mae aparecer
	*timer_appear += 0.1;
	if (*timer_appear > time_mothership) {
		*timer_appear = 0;
		*to_appear = !(*to_appear);
	}

	//timer para aparecer os pontos da nave mae quando atingida
	if (mothership->life == 0 && *timer_points < 5) {
		*timer_points += 0.1;
	} else {
		mothership->life = 1;
		*timer_points = 0;
	}
}

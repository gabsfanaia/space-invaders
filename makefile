TARGET=a.out
CC=gcc
DEBUG=-g
OPT=-O0
WARN=-Wall
ALLEGRO=-lallegro -lallegro_ttf -lallegro_image -lallegro_font

CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(ALLEGRO)
LD=gcc
OBJS= main.o ship.o enemy.o obstacle.o mothership.o additional.o shoots_and_collision.o

all: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(CCFLAGS)
	@rm *.o
	@./$(TARGET)

main.o: main.c
	$(CC) -c $(CCFLAGS) main.c -o main.o

ship.o: ship/ship.c
	$(CC) -c $(CCFLAGS) ship/ship.c -o ship.o

enemy.o: enemy/enemy.c
	$(CC) -c $(CCFLAGS) enemy/enemy.c -o enemy.o

obstacle.o: obstacle/obstacle.c
	$(CC) -c $(CCFLAGS) obstacle/obstacle.c -o obstacle.o

mothership.o: mothership/mothership.c
	$(CC) -c $(CCFLAGS) mothership/mothership.c -o mothership.o

additional.o: additional/additional.c
	$(CC) -c $(CCFLAGS) additional/additional.c -o additional.o

shoots_and_collision.o: shoots_and_collision/shoots_and_collision.c
	$(CC) -c $(CCFLAGS) shoots_and_collision/shoots_and_collision.c -o shoots_and_collision.o

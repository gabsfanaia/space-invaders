#ifndef _OBSTACLE_
#define _OBSTACLE_

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>

#define N_OBSTACLES 3

struct obstacle {
	double center_w, center_h;
	int damage;
	int f;
};

void init_obstacle(struct obstacle *obstacle);
void draw_obstacle(ALLEGRO_BITMAP *obstacle, struct obstacle *obstacles);
#endif

#include "obstacle.h"

#define MAX_OBSTACLES 3

/*draw_obstacle - imprimi o obstáculo no mapa*/
void draw_obstacle(ALLEGRO_BITMAP *obstacle, struct obstacle *obstacles) {
	for (int i = 0; i < MAX_OBSTACLES; i++) {
		al_draw_bitmap_region(obstacle, 0 + obstacles[i].f * 125, 110, 120, 100, obstacles[i].center_w, obstacles[i].center_h, 0);
	}
}

/*init_obstacle - inicializa o obstaculo com seu ponto no mapa e dano causado
 * a estrutura*/
void init_obstacle(struct obstacle *obstacle) {
	int w = 100;
	int h = 370;
	for (int i = 0; i < N_OBSTACLES; i++) {
		obstacle[i].damage = 10;
		obstacle[i].center_w = w;
		obstacle[i].center_h = h;
		//controla a frame dos obstaculos conforme o dano
		obstacle[i].f = 0;
		w += 320;
	}
}
